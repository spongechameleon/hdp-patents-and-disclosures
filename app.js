require('dotenv').config();

// IMPORTS
// deps
const express = require('express'),
    app = express(),
    fs = require('fs'),
    mongoose = require('mongoose'),
    seedDB = require('./seed'),
    sgMail = require('@sendgrid/mail'),
    aws = require('aws-sdk'),
    methodOverride = require('method-override'),
    cors = require('cors'),
    sessions = require('client-sessions'),
    helmet = require('helmet');

// SECURITY
app.use(helmet());
app.use(cors());

// MONGO
mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(() => {
    console.log('Connected to DB');
}).catch(err => {
    console.log(`Mongo connection error:\n${err}`);
});

// AWS
// aws credentials kept in ~/.aws/credentials (local) or env variables (heroku)
aws.config.update({ region: 'us-east-2' });

// SETTINGS
// views
app.use(express.static(`${__dirname}/public`));
app.set('view engine', 'ejs');
app.use(methodOverride('_method'));

// DEV ENVIRONMENT ONLY
if (process.env.DEV === "true") {
    seedDB();

    app.use(sessions({
        cookieName: 'session',
        secret: process.env.COOKIE_SECRET,
        duration: process.env.COOKIE_DURATION, // 1 day
        cookie: {
            ephemeral: process.env.COOKIE_EPHEMERAL,
            path: process.env.COOKIE_PATH,
            httpOnly: process.env.COOKIE_HTTP_ONLY,
            secure: process.env.COOKIE_SECURE,
            domain: process.env.COOKIE_DOMAIN,
        }
    }));
} else {
    app.use(sessions({
        cookieName: 'session',
        secret: process.env.COOKIE_SECRET,
        duration: process.env.COOKIE_DURATION, // 1 day
        cookie: {
            ephemeral: process.env.COOKIE_EPHEMERAL,
            path: process.env.COOKIE_PATH,
            httpOnly: process.env.COOKIE_HTTP_ONLY,
            // secure: process.env.COOKIE_SECURE,
            domain: process.env.COOKIE_DOMAIN,
        }
    }));
}


// pass domain to all pages
app.use((req, res, next) => {
    res.locals.domain = process.env.CUSTOMER_DOMAIN;
    res.locals.errorMessage = undefined;
    res.locals.message = undefined;
    next();
});

// email tx
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

// routes
app.get('/', (req, res) => {
    res.render('landing');
});

const disclosureRoutes = require('./routes/disclosures'),
    patentRoutes = require('./routes/patents'),
    productRoutes = require('./routes/products'),
    adminRoutes = require('./routes/admin');

app.use('/disclosures', disclosureRoutes);
app.use('/patents', patentRoutes);
app.use('/products', productRoutes);
app.use('/admin', adminRoutes);

// SERVER START
if (process.env.DEV === "true") {
    const https = require('https');

    const options = {
        key: fs.readFileSync(process.env.SSL_KEY, 'utf8'),
        cert: fs.readFileSync(process.env.SSL_CERT, 'utf8')
    };

    const httpsServer = https.createServer(options, app);

    httpsServer.listen(process.env.PORT, process.env.IP, () => {
        console.log('\x1b[41m%s\x1b[0m', 'https server is up!');
    });
} else {
    app.listen(process.env.PORT, process.env.IP, () => {
        console.log('\x1b[41m%s\x1b[0m', 'http server is up!');
    });
}


