import shutil
import os

# Functions
def name_refactor_both(file, pathUpTo):
    with open(f"{pathUpTo}/new_file", 'w') as new_file, open(f"{pathUpTo}/{file}", 'r') as target:
        for line in target:
            line2 = line.replace(src_fullname, dest_fullname)
            line2 = line2.replace(src_name.upper(), dest_name.upper())
            line2 = line2.replace(src_name, dest_name)
            new_file.write(line2)

    os.remove(f"{pathUpTo}/{file}")
    os.rename(f"{pathUpTo}/new_file", f"{pathUpTo}/{file}")


def name_refactor_only_fullname(file, pathUpTo):
    with open(f"{pathUpTo}/new_file", 'w') as new_file, open(f"{pathUpTo}/{file}", 'r') as target:
        for line in target:
            line2 = line.replace(src_fullname, dest_fullname)
            new_file.write(line2)

    os.remove(f"{pathUpTo}/{file}")
    os.rename(f"{pathUpTo}/new_file", f"{pathUpTo}/{file}")

# Copy directory
while True:
    try:
        src_path = input('Source directory path: ')
        src_name = input('Source folder name: ')
        src_fullname = input('Source company name: ')
        dest_path = input('New directory path: ')
        dest_name = input('New folder name: ')
        dest_fullname = input('New company name: ')

        shutil.copytree(src_path, dest_path)
        break

    except:
        print("Error: Please re-enter proper directory names")
        continue

# Remove git
shutil.rmtree(f"{dest_path}/.git")

# Name swapping
## Really just the views and controllers/disclosures.js files that need swapping done
partialPaths = [f"{dest_path}/views/disclosures/partials",
                f"{dest_path}/views/patents/partials"]
viewPaths = [f"{dest_path}/views/disclosures",
             f"{dest_path}/views/patents", f"{dest_path}/views/products"]


dirPaths = partialPaths + viewPaths

for p in dirPaths:
    files = os.listdir(p)
    # filter out 'partials' directory from views paths
    files = [x for x in files if x != "partials"]
    for f in files:
        name_refactor_both(f, p)

name_refactor_both('.env', f"{dest_path}")

name_refactor_only_fullname('disclosures.js', f"{dest_path}/controllers")

# Git reminder
print('All done')
print("Don't forget to:")
print('git init')
print('new mongodb db')
print('new aws buckets')
