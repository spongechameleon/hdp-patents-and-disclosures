const fs = require('fs'),
    codes = require('http-status-codes'),
    Patent = require('../models/patent'),
    aws = require('aws-sdk'),
    s3 = new aws.S3({ apiVersion: 'latest' }),
    Product = require('../models/product');

exports.getAllPatents = (req, res) => {
    Product.find({}).populate('patents').exec((err, products) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/index', { errorMessage: 'Cannot access database' });
        } else if (req.session.manageMode) {
            return res.render('patents/index', { manageMode: req.session.manageMode, products });
        } else {
            res.render('patents/index', { manageMode: undefined, products });
        }
    });
};

exports.getNewPatent = (req, res) => {
    Product.find({}, (err, products) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/new', { errorMessage: 'Cannot access database' });
        } else {
            res.render('patents/new', { products });
        }
    });
};

exports.postPatent = (req, res) => {
    Patent.create(req.body, async function (err, patent) {
        if (err) {
            return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/new', { errorMessage: `Cannot access database` });
        } else {
            // file upload
            for (let file of req.files) {
                // add attachment info to db
                await patent.attachment.push({ filePath: file.path, fileName: file.filename, originalName: file.originalname, mimeType: file.mimetype, size: file.size });

                // upload to aws
                let uploadParams = { Bucket: process.env.AWS_PATENT_BUCKET, Key: file.filename, Body: '' }
                let fileStream = fs.createReadStream(file.path);
                fileStream.on('error', err => console.log('File error', err));
                uploadParams.Body = fileStream;
                s3.upload(uploadParams, (err) => {
                    if (err) {
                        console.log(err);
                        return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/new', { errorMessage: 'AWS upload error' });
                    }
                });
            }
            await patent.save();
            // link to product
            Product.find({ _id: req.body.products }, async function (err, products) {
                if (err) {
                    console.log(err);
                    return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/new', { errorMessage: `Cannot access database` });
                } else {
                    try {
                        // TODO: Handle ObjectID cast error.. and unhandled promise rejection???
                        for await (let product of products) {
                            product.patents.push(patent);
                            product.save();
                        }
                        return res.render('patents/submitted', { patent });
                    } catch (err) {
                        console.log(err);
                        return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/new', { errorMessage: `Cannot access database` });
                    }
                }
            });
        }
    });
};

exports.getPatent = (req, res) => {
    Patent.findById(req.params.id, (err, patent) => {
        if (err || !patent) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/index', { errorMessage: 'Cannot access database, or patent does not exist' });
            // for edit & delete views
        } else if (req.session.manageMode) {
            return res.render('patents/show', { manageMode: req.session.manageMode, patent });
        } else {
            res.render('patents/show', { manageMode: undefined, patent });
        }
    });
};

exports.getEditPatent = (req, res) => {
    Patent.findById(req.params.id, (err, patent) => {
        if (err || !patent) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/show', { errorMessage: 'Cannot access database' });
        } else {
            res.render('patents/edit', { patent });
        }
    });
}

exports.getUpdatePatent = (req, res) => {
    Patent.findByIdAndUpdate(req.params.id, req.body, (err, patent) => {
        if (err || !patent) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/show', { errorMessage: 'Cannot access database' });
        } else {
            // find existing attachment names
            let existingNames = [];
            for (let attachment of patent.attachment) {
                existingNames.push(attachment.originalName);
            }
            // only add uniquely new attachments
            for (let file of req.files) {
                if (patent.attachment.length < 4 && !existingNames.includes(file.originalname)) {
                    patent.attachment.push(file);
                }
            }
            patent.save();
            res.redirect(`/patents/${patent._id}`);
        }
    });
};

exports.deletePatent = (req, res) => {
    Patent.findByIdAndDelete(req.params.id, (err, removedPatent) => {
        if (err || !removedPatent) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/show', { errorMessage: 'Cannot access database' });
        } else {
            res.redirect('/patents');
        }
    });
};

exports.getDownloadPatentAttachment = (req, res) => {
    Patent.findById(req.params.id, (err, patent) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/show', { errorMessage: `Cannot access database, or patent does not exist`, patent });
        } else {
            for (let attachment of patent.attachment) {
                if (attachment.fileName === req.params.filename) {
                    res.attachment(attachment.fileName);
                    const fileStream = s3.getObject({ Bucket: process.env.AWS_PATENT_BUCKET, Key: attachment.fileName }).createReadStream();
                    return fileStream.pipe(res);
                }
            }
            return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/show', { errorMessage: 'attachment does not exist' });
        }
    });
};