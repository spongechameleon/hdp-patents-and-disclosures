const codes = require('http-status-codes'),
    auth = require('../functions/auth'),
    helpers = require('../functions/helpers'),
    sgMail = require('@sendgrid/mail'),
    User = require('../models/user');

exports.getLogin = (req, res) => {
    // if not logged in
    if (!(req.session && req.session.userId)) {
        return res.render('admin/login', { message: undefined });
    }

    // if logged in
    res.redirect('/admin/dashboard');
};

exports.postLogin = async function (req, res) {
    // make jwt
    let token = auth.generateJWT(req.fullEmail);
    // send e-mail
    let msg = {
        to: req.fullEmail,
        // TODO: USE OWN E-MAIL SERVER
        from: `admin@mcorp.com`,
        template_id: 'd-552ae3175c674845b82e2214d2d5e88c',
        dynamic_template_data: {
            link: `${helpers.fullUrl(req)}/../landing?token=${token}`
        }
    };
    try {
        await sgMail.send(msg, false, err => {
            if (err) {
                console.log(err);
                return res.status(codes.INTERNAL_SERVER_ERROR).render('admin/login', { errorMessage: 'Cannot send email' });
            }
            return res.render('admin/login', { message: 'E-mail sent! Please check your inbox.' });
        });
    } catch (err) {
        console.log(err);
    }
};

exports.getLanding = (req, res) => {
    // if you made it this far, you've authenticated!!
    // User db check
    User.find({ email: req.user }, (err, user) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('admin/landing', { errorMessage: 'Cannot access database' });
        }
        // if new
        if (user.length === 0) {
            User.create({ email: req.user }, (err, newUser) => {
                if (err) {
                    console.log(err);
                    return res.status(codes.INTERNAL_SERVER_ERROR).render('admin/landing', { errorMessage: 'Cannot access database' });
                } else {
                    req.session.userId = newUser.email;
                    return res.render('admin/landing', { user: req.session.userId });
                }
            });
            // if existing user
        } else {
            req.session.userId = user[0].email;
            return res.render('admin/landing', { user: req.session.userId });
        }
    });
};

exports.getDashboard = (req, res) => {
    req.session.manageMode = undefined;
    res.render('admin/dashboard', { user: req.session.userId });
};

exports.postDashboard = (req, res) => {
    // set cookie value
    if (req.body.manageMode === 'edit') {
        req.session.manageMode = 'edit';
    } else if (req.body.manageMode === 'delete') {
        req.session.manageMode = 'delete';
    }

    // redirect 
    if (req.session.manageMode) {
        return res.redirect('/patents');
    }
    res.redirect('/admin/dashboard');
};

exports.getLogout = (req, res) => {
    res.clearCookie('session');
    res.redirect('/patents');
};