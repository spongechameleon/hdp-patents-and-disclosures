const Disclosure = require('../models/disclosure'),
    sgMail = require('@sendgrid/mail'),
    fs = require('fs'),
    aws = require('aws-sdk'),
    s3 = new aws.S3({ apiVersion: 'latest' }),
    codes = require('http-status-codes');

exports.getNewDisclosure = (req, res) => {
    res.render('disclosures/new');
};

exports.postDisclosure = (req, res) => {
    // create db entry
    Disclosure.create(req.body, (err, disclosure) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('disclosures/new', { errorMessage: 'Cannot access database' });
        } else {
            if (req.files.length > 0) {
                // add attachment info to db
                disclosure.attachment.push({ filePath: req.files[0].path, fileName: req.files[0].filename, originalName: req.files[0].originalname, mimeType: req.files[0].mimetype, size: req.files[0].size });
                disclosure.save();

                // upload to aws
                let uploadParams = { Bucket: process.env.AWS_DISCLOSURE_BUCKET, Key: req.files[0].filename, Body: '' }
                let fileStream = fs.createReadStream(req.files[0].path);
                fileStream.on('error', err => console.log('File error', err));
                uploadParams.Body = fileStream;
                s3.upload(uploadParams, (err) => {
                    if (err) {
                        console.log(err);
                        return res.status(codes.INTERNAL_SERVER_ERROR).render('disclosures/new', { errorMessage: 'Cannot access database' });
                    }
                });
            }
            // send confirmation email (production only!)
            const msg = {
                to: `${disclosure.author.email}`,
                from: `noreply@mcorp.com`,
                subject: 'Thanks for submitting your idea',
                html: `<p>${disclosure.author.firstName},</p><p>Thank you for submitting your idea to M Corporation.</p><p>M Corporation will contact you within 90 days with a decision. If, after thorough consideration, M Corporation elects to pursue your contribution further, instructions on next steps will be provided at that time.</p><p>We once again appreciate your submission.</p>`
            };
            if (process.env.DEV === 'false') {
                sgMail.send(msg);
            }
            res.redirect('/disclosures/submitted');
        }
    });
};

exports.getAllDisclosures = (req, res) => {
    Disclosure.find({}, (err, disclosures) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('disclosures/index', { errorMessage: 'Cannot access database' });
        } else {
            res.render('disclosures/index', { disclosures });
        }
    });
};

exports.getDisclosure = (req, res) => {
    Disclosure.findById(req.params.id, (err, disclosure) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('disclosures/show', { errorMessage: 'Cannot access database or disclosure does not exist' });
        } else {
            res.render('disclosures/show', { disclosure });
        }
    });
};

exports.putUpdateDisclosure = (req, res) => {
    Disclosure.findByIdAndUpdate(req.params.id, req.body, (err) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('disclosures/show', { errorMessage: 'Cannot access database or disclosure does not exist' });
        } else {
            res.redirect(`/disclosures/${req.params.id}`);
        }
    });
};

exports.deleteDisclosure = (req, res) => {
    Disclosure.findByIdAndDelete(req.params.id, (err) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('dislcosures/show', { errorMessage: 'Cannot access database or disclosure does not exist' });
        } else {
            res.redirect('/disclosures');
        }
    });
};

exports.getDownloadDisclosureAttachment = (req, res) => {
    Disclosure.findById(req.params.id, (err, disclosure) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('disclosures/show', { errorMessage: 'Cannot access database or disclosure does not exist' });
        } else {
            res.attachment(disclosure.attachment[0].fileName);
            const fileStream = s3.getObject({ Bucket: process.env.AWS_DISCLOSURE_BUCKET, Key: disclosure.attachment[0].fileName }).createReadStream();
            return fileStream.pipe(res);
        }
    });
};