const codes = require('http-status-codes'),
    Patent = require('../models/patent'),
    Product = require('../models/product');

exports.getAllProducts = (req, res) => {
    Product.find({}, (err, products) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('products/index', { errorMessage: 'Cannot access database', products });
        } else {
            res.render('products/index', { products });
        }
    });
};

exports.getNewProduct = (req, res) => {
    Patent.find({}, (err, patents) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('products/new', { error: 'Cannot access database', patents });
        } else {
            res.render('products/new', { patents });
        }
    });
};

exports.postProduct = (req, res) => {
    Product.create({ name: req.body.name, description: req.body.description, releaseYear: req.body.releaseYear }, (err, product) => {
        if (err) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('products/new', { errorMessage: 'Cannot access database' });
        } else {
            Patent.find({ _id: req.body.patents }, (err, patents) => {
                if (err) {
                    console.log(err);
                    return res.status(codes.INTERNAL_SERVER_ERROR).render('products/new', { errorMessage: 'Cannot access database' });
                } else {
                    for (let patent of patents) {
                        product.patents.push(patent);
                    }
                    product.save();
                    return res.redirect('/admin/dashboard');
                }
            });
        }
    });
};

exports.getEditProduct = (req, res) => {
    Product.findById(req.params.id, (err, product) => {
        if (err || !product) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('products/edit', { error: 'Cannot access database', product });
        } else {
            Patent.find({}, (err, patents) => {
                if (err) {
                    console.log(err);
                    return res.status(codes.INTERNAL_SERVER_ERROR).render('products/edit', { errorMessage: 'cannot access database', product });
                } else {
                    res.render('products/edit', { product, patents });
                }
            });
        }
    });
};

exports.putUpdateProduct = (req, res) => {
    Product.findByIdAndUpdate(req.params.id, { name: req.body.name, description: req.body.description, releaseYear: req.body.releaseYear }, async function (err, product) {
        if (err || !product) {
            console.log(err);
            return res.status(codes.INTERNAL_SERVER_ERROR).render('products/edit', { errorMessage: 'Cannot access database' });
        } else {
            // update linked patents
            try {
                product.patents = [];
                for await (let patent of req.body.patents) {
                    product.patents.push(patent);
                }
                await product.save();
                res.redirect('/admin/dashboard');
            } catch (err) {
                console.log(err);
                return res.status(codes.INTERNAL_SERVER_ERROR).render('products/edit', { errorMessage: 'cannot access database', product });
            }
        }
    });
};

exports.deleteProduct = (req, res) => {
    Product.findByIdAndDelete(req.params.id, (err, removedProduct) => {
        if (err || !removedProduct) {
            return res.status(codes.INTERNAL_SERVER_ERROR).render('products/edit', { errorMessage: 'Cannot access database', product: removedProduct });
        } else {
            res.redirect('/patents');
        }
    });
};