const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: { $type: String, unique: true, required: true },
    patents: [{
        $type: mongoose.Schema.Types.ObjectId,
        ref: 'Patent',
    }],
    description: String,
    releaseYear: Number
},
    { typeKey: '$type' }
);

module.exports = mongoose.model('Product', productSchema);