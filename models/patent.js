const mongoose = require('mongoose');

const patentSchema = new mongoose.Schema({
    contact: {
        firstName: String,
        lastName: String,
        email: String
    },
    attachment: [
        {
            size: Number,
            filePath: String,
            fileName: String,
            originalName: String,
            mimeType: String
        },
    ],
    number: { $type: String, unique: true, required: true },
    title: String,
    description: String,
    grantDate: { $type: String, required: true },
    expirationDate: { $type: String, required: true },
    postDate: Date,
},
    { typeKey: '$type' }
);

module.exports = mongoose.model('Patent', patentSchema);