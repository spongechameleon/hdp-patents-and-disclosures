const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: { $type: String, required: true, unique: true },
},
    { typeKey: '$type' }
);

module.exports = mongoose.model('User', userSchema);