const mongoose = require('mongoose');

const disclosureSchema = new mongoose.Schema({
    author: {
        firstName: String,
        lastName: String,
        email: String,
        address1: String,
        address2: String,
        city: String,
        state: String,
        zip: String,
        country: String,
        phone: String
    },
    attachment: [
        {
            size: Number,
            filePath: String,
            fileName: String,
            originalName: String,
            mimeType: String
        },
    ],
    title: { $type: String, required: true, unique: true },
    description: String,
    classification: String,
    type: String,
    country: String,
    patentPending: { $type: Boolean, required: true },
    patentNumber: String,
    submitDate: Date,
    response: Boolean
},
    { typeKey: '$type' }
);

module.exports = mongoose.model('Disclosure', disclosureSchema);
