# Patents and Disclosures

[Live website](https://hdp-patents.herokuapp.com/)

This project is two sites in one, representing a fictional company's: (a) publicly listed patents (with management portal) and (b) a page for consumers to submit intellectual property disclosures.

The patents site offers a place for the public to verify any of company's held patents. In the footer, the 'Corporate Patent Management' button will lead employees to a login portal by which users can authenticate via their work email- magic link authentication using JWTs- and then perform CRUD operations (create, read, update, destroy) on the public patent data.

The disclosures site offers a method by which the public is able to submit intellectual property suggestions to a company after having agreed to the pertinent terms and conditions. This site is intentionally repetitive and involves a lot of clicking in order to discourage frivolous suggestions (only serious inquiries are desired). 