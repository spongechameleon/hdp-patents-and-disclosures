const express = require('express'),
    router = express.Router(),
    auth = require('../functions/auth'),
    forms = require('../functions/forms'),
    controller = require('../controllers/products');

// index
router.get('/', auth.verifySession, controller.getAllProducts);

// new
router.get('/new', auth.verifySession, controller.getNewProduct);

// create
router.post('/', auth.verifySession, forms.noFiles, controller.postProduct);

// edit
router.get('/:id/edit', auth.verifySession, controller.getEditProduct);

// update
router.put('/:id', auth.verifySession, forms.noFiles, controller.putUpdateProduct);

// destroy
router.delete('/:id', auth.verifySession, controller.deleteProduct);

module.exports = router;