const express = require('express'),
    router = express.Router(),
    auth = require('../functions/auth'),
    forms = require('../functions/forms'),
    controller = require('../controllers/disclosures');

// got an idea?
router.get('/got-an-idea', (req, res) => {
    res.render('disclosures/got-an-idea');
});

// notice
router.get('/notice', (req, res) => {
    res.render('disclosures/notice');
});

// new
router.get('/new', controller.getNewDisclosure);

// create
router.post('/', forms.disclosureCheck, controller.postDisclosure);

// submitted
router.get('/submitted', (req, res) => {
    res.render('disclosures/submitted');
});

// index
router.get('/', auth.verifySession, controller.getAllDisclosures);

// show (and edit/delete)
router.get('/:id', auth.verifySession, controller.getDisclosure);

// update
router.put('/:id', auth.verifySession, forms.noFiles, controller.putUpdateDisclosure);

// destroy
router.delete('/:id', auth.verifySession, controller.deleteDisclosure);

// download attachment
router.get('/:id/:filename', auth.verifySession, controller.getDownloadDisclosureAttachment);

module.exports = router;