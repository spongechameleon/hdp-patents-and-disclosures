const express = require('express'),
    router = express.Router(),
    auth = require('../functions/auth'),
    forms = require('../functions/forms'),
    controller = require('../controllers/patents');

// index
router.get('/', controller.getAllPatents);

// new
router.get('/new', auth.verifySession, controller.getNewPatent);

// create
router.post('/', auth.verifySession, forms.patentCheck, controller.postPatent);

// show
router.get('/:id', controller.getPatent);

// edit
router.get('/:id/edit', auth.verifySession, controller.getEditPatent);

// update
router.put('/:id', auth.verifySession, forms.patentCheck, controller.getUpdatePatent);

// destroy
router.delete('/:id', auth.verifySession, controller.deletePatent);

// download attachment
router.get('/:id/:filename', controller.getDownloadPatentAttachment);

module.exports = router;