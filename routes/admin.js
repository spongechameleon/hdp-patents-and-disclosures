const express = require('express'),
    router = express.Router(),
    auth = require('../functions/auth'),
    forms = require('../functions/forms'),
    controller = require('../controllers/admin');

// login
router.get('/login', controller.getLogin);

// login post & magic jwt link
router.post('/login', forms.noFiles, forms.loginEmailCheck, controller.postLogin);

// magic jwt link landing & session cookie creation
router.get('/landing', auth.verifyLinkJWT, controller.getLanding);

// manage dashboard
router.get('/dashboard', auth.verifySession, controller.getDashboard);

// manage dashboard edit/delete
router.post('/dashboard', auth.verifySession, forms.noFiles, controller.postDashboard);

// logout
router.get('/logout', controller.getLogout);

module.exports = router;