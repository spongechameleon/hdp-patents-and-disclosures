const multer = require('multer'),
    patentUpload = multer({ dest: 'public/files/patents', limits: { fileSize: 4 * 1024 * 1024 } }).array('attachment', 3),
    disclosureUpload = multer({ dest: 'public/files/disclosures', limits: { fileSize: 2 * 1024 * 1024 } }).array('attachment', 1),
    regUpload = multer({ limits: { files: 0 } }).none(),
    codes = require('http-status-codes'),
    validator = require('validator');


const forms = {

    noFiles: function (req, res, next) {
        regUpload(req, res, () => { return next(); });
    },

    loginEmailCheck: function (req, res, next) {
        req.fullEmail = validator.normalizeEmail(validator.escape(validator.trim(`${req.body.email}@${process.env.CUSTOMER_DOMAIN}`)));

        if (validator.isEmpty(req.body.email) || !validator.isEmail(req.fullEmail)) {
            return res.status(codes.UNPROCESSABLE_ENTITY).render('admin/login', { errorMessage: 'Please enter a valid email' });
        }

        return next();
    },

    patentCheck: function (req, res, next) {
        patentUpload(req, res, (error) => {
            // multer errors
            if (error instanceof multer.MulterError) {
                return res.status(codes.BAD_REQUEST).render('patents/new', { errorMessage: error });
            } else if (error) {
                console.log(error);
                return res.status(codes.INTERNAL_SERVER_ERROR).render('patents/new', { errorMessage: 'Cannot access database' });
            }

            // check if files are pdf
            if (req.files.length > 0) {
                for (let file of req.files) {
                    if (file.mimetype !== 'application/pdf' || file.mimetype !== 'application/zip') {
                        return res.status(codes.UNPROCESSABLE_ENTITY).render('patents/new', { errorMessage: 'Only .pdf and .zip files accepted' });
                    } else if (file.size >= 4194304) {
                        return res.status(codes.UNPROCESSABLE_ENTITY).render('patents/new', { errorMessage: 'File size must be < 4MB' });
                    }
                }
            }

            // sanitize & check for req'd fields
            const required = ['contact.email', 'contact.firstName', 'contact.lastName', '_id', 'title', 'grantDate', 'expirationDate', 'postDate'];
            for (let field in req.body) {
                if (required.includes(field) && validator.isEmpty(req.body[field])) {
                    return res.status(codes.UNPROCESSABLE_ENTITY).render('patents/new', { errorMessage: `Missing required field ${field}` });
                } else {
                    req.body[field] = validator.escape(validator.trim(req.body[field]));
                }
            }

            // check email
            req.body['contact.email'] = validator.normalizeEmail(validator.escape(validator.trim(req.body['contact.email'])));
            if (!validator.isEmail(req.body['contact.email'])) {
                return res.status(codes.UNPROCESSABLE_ENTITY).render('patents/new', { errorMessage: 'Improper email' });
            }

            return next();
        });
    },

    disclosureCheck: function (req, res, next) {
        disclosureUpload(req, res, (error) => {
            // multer errors
            if (error instanceof multer.MulterError) {
                return res.status(codes.BAD_REQUEST).render('disclosures/new', { errorMessage: error });
            } else if (error) {
                console.log(error);
                return res.status(codes.INTERNAL_SERVER_ERROR).render('disclosures/new', { errorMessage: 'Cannot access database' });
            }

            // check if files are pdf
            if (req.files.length > 0) {
                for (let file of req.files) {
                    if (file.mimetype !== 'application/pdf' || file.mimetype !== 'application/zip') {
                        return res.status(codes.UNPROCESSABLE_ENTITY).render('disclosures/new', { errorMessage: 'Only .pdf and .zip files accepted' });
                    } else if (file.size >= 2097152) {
                        return res.status(codes.UNPROCESSABLE_ENTITY).render('disclosures/new', { errorMessage: 'File size must be <= 2MB' });
                    }
                }
            }

            // check required fields
            const required = ['author.email', 'author.firstName', 'author.lastName', 'terms', 'classification', 'title', 'description', 'patentPending', 'type', 'submitDate'];
            for (let field in req.body) {
                if (required.includes(field) && validator.isEmpty(req.body[field])) {
                    return res.status(codes.UNPROCESSABLE_ENTITY).render('disclosures/new', { errorMessage: `Missing required field ${field}` });
                } else {
                    req.body[field] = validator.escape(validator.trim(req.body[field]));
                }
            }

            // check email
            req.body['author.email'] = validator.normalizeEmail(validator.escape(validator.trim(req.body['author.email'])));
            if (!validator.isEmail(req.body['author.email'])) {
                return res.status(codes.UNPROCESSABLE_ENTITY).render('disclosures/new', { errorMessage: 'Improper email' });
            }

            return next();
        });
    },

}

module.exports = forms;