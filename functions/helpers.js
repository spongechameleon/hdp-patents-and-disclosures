const url = require('url');

const helpers = {

    fullUrl: function (req) {
        return url.format({
            protocol: req.protocol,
            host: req.get('host'),
            pathname: req.originalUrl
        });
    },

}

module.exports = helpers;