const jwt = require('jsonwebtoken'),
    codes = require('http-status-codes'),
    url = require('url');

const auth = {

    generateJWT: function (email) {
        return jwt.sign({ email }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_DURATION });
    },

    verifyLinkJWT: function (req, res, next) {
        const token = url.parse(req.url, true).query.token;
        if (!token) {
            res.status(codes.FORBIDDEN).render('admin/login', { errorMessage: 'Unable to login' });
            return;
        }

        let decoded;
        try {
            decoded = jwt.verify(token, process.env.JWT_SECRET);
        } catch (err) {
            console.log(err);
            res.status(codes.FORBIDDEN).render('admin/login', { errorMesasge: 'Cannot verify login' });
            return;
        }

        if (!Object.prototype.hasOwnProperty.call(decoded, 'email')) {
            res.status(codes.FORBIDDEN).render('admin/login', { errorMesasge: 'Cannot verify login' });
            return;
        }

        req.user = decoded.email;
        return next();
    },

    verifySession: function (req, res, next) {
        // if not logged in, send to login page
        if (!(req.session && req.session.userId)) {
            return res.redirect('/admin/login');
        }

        // if logged in
        return next();
    },

}

module.exports = auth;