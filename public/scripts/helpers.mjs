// for url grabbing on show page
export function copy() {
    url.select();
    document.execCommand('copy');
}

// create show/hide functions for search/index pages
export function hideElement(elementArray, removeClass) {
    return arrayHardToggle(removeClass, 'hide')(elementArray);
}

export function showElement(elementArray, addClass) {
    return arrayHardToggle('hide', addClass)(elementArray);
}

function arrayHardToggle(rm, add) {
    return function(arr) {
        for (let i = 0; i < arr.length; ++i) {
            arr[i].classList.remove(rm);
            arr[i].classList.add(add);
        }
    }
}