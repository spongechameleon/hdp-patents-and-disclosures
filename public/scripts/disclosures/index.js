import { showElement, hideElement } from '../helpers.mjs';

// showElement & hideElement defined in helpers.js

// Document selectors
const searchField = document.querySelector('.search-bar'),
    contentLis = document.querySelectorAll('ul li'),
    contentUls = document.querySelectorAll('ul'),
    contentUlTitles = document.querySelectorAll('ul li#title-showpage'),
    submitDate = document.querySelectorAll('li#disclosure-submit-date');

// Search with 'Search' field
searchField.addEventListener('keyup', () => {
    // get search phrase
    let searchKey = searchField.value.toLowerCase();
    // if no search phrase
    if (searchKey === '') {
        // show everything
        showElement([...contentUls, ...contentLis], 'show-block');
    } else {
        // for each ul group
        for (let contentUl of contentUls) {
            // if searchKey matches a title (ul > li > a.innerText)
            if (contentUl.children[0].children[0].innerText.toLowerCase().indexOf(searchKey) > -1) {
                // show the content (if not already shown)
                if (contentUl.classList.contains('hide')) {
                    showElement([contentUl, ...contentUl.children], 'show-block');
                }
            } else {
                // hide the content (if not already hidden)
                if (!contentUl.classList.contains('hide')) {
                    hideElement([contentUl, ...contentUl.children], 'show-block');
                }
            }
        }
    }
});