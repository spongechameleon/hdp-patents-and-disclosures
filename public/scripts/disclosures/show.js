import {copy} from '../helpers.mjs';

// copy url thing
const url = document.querySelector('#url'),
    copyBtn = document.querySelector('#copy-url-btn');
url.value = window.location.href;
copyBtn.addEventListener('click', copy);

// delete confirmation
const confirmDeleteBtn = document.querySelector('a#confirm-delete-btn button'),
    deleteBtn = document.querySelector('form button.delete-btn');

deleteBtn.style.display = 'none';
confirmDeleteBtn.addEventListener('click', () => {
    if (deleteBtn.style.display === 'none') {
        deleteBtn.style.display = 'inline-block';
    } else {
        deleteBtn.style.display = 'none';
    }
});
