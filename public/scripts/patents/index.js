import { showElement, hideElement } from '../helpers.mjs';

// Document selectors
const searchField = document.querySelector('.search-bar'),
    productUls = document.querySelectorAll('ul.product-list'),
    productTitles = document.querySelectorAll('ul.product-list li'),
    patentUls = document.querySelectorAll('ul.patent-list'),
    patents = document.querySelectorAll('ul.patent-list a');

// Search with 'Search' field
searchField.addEventListener('keyup', () => {
    // get search phrase
    let searchKey = searchField.value.toLowerCase();
    // if no search phrase
    if (searchKey === '') {
        // show all products
        showElement([...productUls, ...productTitles], 'show-block');
        // hide all patents
        hideElement([...patentUls, ...patents], 'show-block');
    } else {
        // for each product group
        for (let productUl of productUls) {
            // if the searchkey matches a title
            if (productUl.children[0].textContent.toLowerCase().indexOf(searchKey) > -1) {
                // show the matching product (if not already shown)
                if (productUl.classList.contains('hide')) {
                    showElement([productUl, productUl.children[0]], 'show-block');
                }
            } else {
                // hide non-matching products (if not already hidden)
                if (!productUl.classList.contains('hide')) {
                    hideElement([productUl, productUl.children[0]], 'show-block');
                }
            }
            for (let patent of productUl.children[1].children) {
                // if searchkey matches the patent
                if (patent.textContent.toLowerCase().indexOf(searchKey) > -1) {
                    // show patent (and parent product and patent group if hidden)
                    if (productUl.classList.contains('hide')) {
                        showElement([productUl, productUl.children[0], productUl.children[1], patent], 'show-block');
                    } else if (productUl.children[1].classList.contains('hide')) {
                        // show patent (if product shown but patent group hidden)
                        showElement([productUl.children[1], patent], 'show-block');
                    } else {
                        // show patent (if product and patent group already shown)
                        showElement([patent], 'show-block');
                    }
                    // if searchkey doesn't match the patent
                } else {
                    // hide patent (if not already hidden)
                    if (!patent.classList.contains('hide')) {
                        hideElement([patent], 'show-block');
                    }
                }
            }
        }
    }
});

// Show/Hide when Product clicked
for (let productUl of productUls) {
    productUl.addEventListener('click', () => {
        productUl.children[1].classList.toggle('hide');
        for (let patent of productUl.children[1].children) {
            patent.classList.toggle('hide');
            patent.classList.toggle('show-block');
        }
    });
}