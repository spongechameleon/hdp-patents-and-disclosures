// Document selectors
const submitDate = document.querySelector('input.submit-date');

// Auto assign date (yyyy-mm-dd) to form input
let d = new Date();
submitDate.value = d;
