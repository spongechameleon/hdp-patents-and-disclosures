const mongoose = require('mongoose'),
    Disclosure = require('./models/disclosure'),
    Patent = require('./models/patent'),
    User = require('./models/user'),
    Product = require('./models/product');

const disclosures = [
    {
        author: {
            firstName: "Bill",
            lastName: "Ryan",
            email: "bryan@company.com",
        },
        classification: "New Product",
        type: "Invention",
        title: "A new lightbulb that uses dust to bioilluminate",
        description: "blah blah blah",
        patentPending: false,
        response: true,
    },
    {
        author: {
            firstName: "Janet",
            lastName: "Smalls",
            email: "jsmalls4@company.com",
        },
        classification: "New Product",
        type: "Invention",
        title: "Tea that doesn't need to steep and stays warm forever",
        description: "blah blah blah",
        patentPending: true,
        type: "Invention",
        patentNumber: 41568923,
        response: false,
    },
    {
        author: {
            firstName: "Mark",
            lastName: "Balper",
            email: "mark_balper@company.com",
        },
        classification: "New Product",
        type: "Invention",
        title: "Eyeglasses that can zoom",
        description: "blah blah blah",
        patentPending: true,
        country: "France",
        patentNumber: "5389404092",
        response: false,
    },
    {
        author: {
            firstName: "Vika",
            lastName: "O'Shaughnessy",
            email: "v.shag@company.com",
        },
        classification: "New Product",
        type: "Invention",
        title: "Texting with only your thoughts",
        description: "blah blah blah",
        patentPending: false,
        response: false,
    },
];

const patents = [
    {
        contact: {
            firstName: "Boos",
            lastName: "Aller",
            email: "baller@ballsohard.com"
        },
        number: 'TW24798148Q',
        title: 'An improved Pythagorean theorem grounded in the roots of classical basketball geometry',
        description: "Boards fer dayz. We've worked out all the angles over the past few ball sessions and we realized if you have the right form, any J is possible. No spot on the court is safe.",
        grantDate: '0000-01-01',
        expirationDate: '9999-12-30',
    },
    {
        contact: {
            firstName: "Dani",
            lastName: "Chica",
            email: "dani@lachica.com"
        },
        number: 'VZ21748982Y',
        title: 'Un gatito de los mejores poder en todo el mundo',
        description: 'No es un idea, es una realidad. El gatito es el jefe del mundo, en serio. Vamoooo!',
        grantDate: '2018-09-14',
        expirationDate: '2023-03-01',
    },
    {
        contact: {
            firstName: "Bill",
            lastName: "O'Reilly",
            email: "bill@smallcompany.net"
        },
        number: 'US3487219B',
        title: 'Propulsion via residual atmospheric nitrous oxide',
        description: 'Our novel engine block captures ambient nitrous oxide for use in passive combustion facilitated by enzymatic overload of nitrogen groups',
        grantDate: '2017-11-24',
        expirationDate: '2022-11-24',
    },
    {
        contact: {
            firstName: "Todo",
            lastName: "Glteck",
            email: "runningout@ofemails.com"
        },
        number: 'RS27497890A',
        title: 'AA lithium-ion batteries with an exceptionally useful lifetime',
        description: "The ions just keep going forever and ever and ever and ever. It's a secret so we're not going to tell anybody how we did it.",
        grantDate: '2019-01-02',
        expirationDate: '2025-03-08',
    },
    {
        contact: {
            firstName: "Maxine",
            lastName: "Farrough",
            email: "mfarrough7@bigcompany.com"
        },
        number: 'US2331422F',
        title: 'Temperature independence of viscous fluids by polymerase adsorption',
        description: 'Viscosity of fluids not affected by temperature variations, due to secret helix manipulation technique. Pending FDA approval for human consumables.',
        grantDate: '2013-02-10',
        expirationDate: '2018-02-10',
    },
    {
        contact: {
            firstName: "La",
            lastName: "Gente",
            email: "lagente@elmundo.es"
        },
        number: 'ES72987589G',
        title: 'Cardboard thermometer accurate to two hundredths of a degree celsius',
        description: 'Using only cardboard this device can be used without any electronics to provide a manual readout of any surface temperature (including that of humans and other mammals). This device is made from 100% recycled cardboard.',
        grantDate: '1956-07-19',
        expirationDate: '2017-11-12',
    },
    {
        contact: {
            firstName: "Kevin",
            lastName: "DeMasia",
            email: "kevin_demasia@mailbox.com"
        },
        number: 'US9876190A',
        title: 'Audio sensitive and manipulative hygrometer micro-controllers',
        description: 'Sweat levels are measured with advanced hygrometers. Propietary software (patent no. US423761Y) is used to adjust the gain and other musical characteristics of the audio to suit the perceived vibe.',
        grantDate: '2015-04-15',
        expirationDate: '2020-04-15',
    },
    {
        contact: {
            firstName: "Hedge",
            lastName: "Hog",
            email: "sonic@zooom.com"
        },
        number: 'PE274891110809J',
        title: 'Tungsten needles of remarkable strength and durability',
        description: 'Flexible but not easily broken. Tensile strength out the waz. Very light and made of tungsten arranged of unique hexadecimal crystallinity.',
        grantDate: '2011-06-06',
        expirationDate: '2016-12-06',
    },
    {
        contact: {
            firstName: "Sinead",
            lastName: "Shaughnessy",
            email: "sinead@mailbox.com"
        },
        number: 'CA0087136T',
        title: 'Intelligent weight scale re-purposed for garbage that sends weekly reports to your e-mail',
        description: 'Utilizing the same scales that grocery stores use for self checkout, these bins track user trash habits on a weekly basis and broadcast this data not only to the user but to M Corporation and all of our business partners.',
        grantDate: '2020-03-02',
        expirationDate: '2025-03-02',
    },
    {
        contact: {
            firstName: "Streamer",
            lastName: "Bob",
            email: "bob@streamer.com"
        },
        number: 'NZ48209748S',
        title: '500GB/s wifi access point',
        description: 'Serves up some serious bandwidth without any of the wires. Utilizes incredible infrared photon transmission properties previously unknown to science.',
        grantDate: '2001-10-23',
        expirationDate: '2039-10-12',
    },
];

const products = [
    {
        name: 'Miscellaneous',
        description: 'Miscellaneous',
        releaseYear: 0000
    },
    {
        name: 'Flying lawnmower',
        description: 'It levitates off of the ground and is able to mow everything in one pass with no effort from the user. Turbo speed mode can be activated by channeling user flatulent through the provided hose attachment. Depending on the "gusto" of the input flatulent, users may experience serious increases in mower speed.',
        releaseYear: 2003
    },
    {
        name: 'Great custard',
        description: 'A 5" x 5" pillowy yoshi decoration',
        releaseYear: 2012
    },
    {
        name: 'Smart DJ Turntable',
        description: 'Vibes with the sweaty crowd, chills with the dry crowd.',
        releaseYear: 2019
    },
    {
        name: 'Mensa-grade trash can',
        description: "A trashcan that's probably smarter than you!",
        releaseYear: 1991
    }
];

async function seedDB() {
    try {
        await Promise.all([Disclosure.remove({}), Patent.remove({}), User.remove({}), Product.remove({})]);

        console.log('Database cleaned :)');

        let d = new Date();

        for (let disclosure of disclosures) {
            let seed = await Disclosure.create(disclosure);
            seed.submitDate = d;
            seed.save();
            console.log(`Created disclosure from: ${seed.author.email}`);
        }

        for (let i = 0; i < products.length; ++i) {
            let product = await Product.create(products[i]);

            let patent = await Patent.create(patents[2 * i]);
            patent.postDate = d;
            patent.save();

            let patent2 = await Patent.create(patents[(2 * i) + 1]);
            patent2.postDate = d;
            patent2.save();

            product.patents.push(patent);
            product.patents.push(patent2);
            product.save();

            console.log(`Created ${product.name} with patents ${patent.number} & ${patent2.number}`);
        }

    } catch (err) {
        console.log(`Seed error:\n${err.stack}`);
    }
}

module.exports = seedDB;